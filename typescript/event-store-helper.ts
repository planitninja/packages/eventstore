'use strict';
import * as extend from 'extend';
import * as crypto from 'crypto';
import { IServiceDependency, ICancellationToken } from 'metis-dependency-manager';
import { IAvailability, CallbackFunction } from 'metis-service-utility';

const forwardUri = { category: '/streams/$ce-{entityName}/{start}/forward/{count}', entity: '/streams/{entityName}-{entityIdentity}/{start}/forward/{count}' };
const backwardsUri = { category: '/streams/$ce-{entityName}/{start}/backward/{count}', entity: '/streams/{entityName}-{entityIdentity}/{start}/backward/{count}' };
const streamUri = { category: '/streams/$ce-{entityName}', entity: '/streams/{entityName}-{entityIdentity}' };
const eventUri = { category: '/streams/$ce-{entityName}/{start}', entity: '/streams/{entityName}-{entityIdentity}/{start}' };


export interface IEvent {
    data: any,
    eventNumber?: number,
    originalEventNumber: number,
    link: string,
    originalLink: string
}

interface IStreamInfo {
    entries: { id: string, updated: string }[],
    links: { uri: string, relation: string }[]
}

interface ICounter {
    next: () => void,
    current: number,
    incomingEntries: (count: number) => void,
    streamRead: () => void;
}

class ForwardCounter implements ICounter {
    skip: number;
    constructor(start: number, streamCount: number) {
        this.current = start;
        this.skip = streamCount;
    }
    streamRead() {
        //this.current += this.skip;
    }

    incomingEntries(count: number): void {
        this.current += count;
    }
    next(): void {
        this.current--;
    }
    current: number;
}

class BackwardsCounter implements ICounter {
    skip: number;
    started: boolean = false;
    constructor(start: number, streamCount: number) {
        this.current = start + 1;
        this.skip = streamCount;
    }
    streamRead() {
        if (this.started == false) {
            this.current -= this.skip;
        }
    }

    incomingEntries(count: number): void {
        if (count > 0 && this.started === false) {
            this.current += count;
            this.started = true;
        }
    }
    next(): void {
        this.current--;
    }
    current: number;
}

function urlBuilder(template: { category: string, entity: string }, params: { entityName: string, entityIdentity?: string, eventNumber?: number, count?: number }): string {
    if (template === undefined || template === null) {
        throw new Error("Entity name required");
    }
    if (params === undefined || params === null) {
        throw new Error("Entity name required");
    }
    if (params.entityName === undefined || params.entityName === null) {
        throw new Error("Entity name required");
    }

    let url = params.entityIdentity ? template.entity : template.category;

    url = url.replace('{entityName}', params.entityName);
    if (params.entityIdentity) {
        url = url.replace('{entityIdentity}', params.entityIdentity);
    }
    let eventNumber = params.eventNumber || 0;
    url = url.replace('{start}', eventNumber.toString());
    let count = params.count || 20;
    url = url.replace('{count}', count.toString());
    return url;
}

interface IEventStoreClientOptions {
    hostname: string,
    port: number
};


interface IStreamReader {
    readForward(start: number, callback: (message: any) => void): Promise<void>;
    readBackward(start: number, callback: (message: any) => void): Promise<void>;
    followFrom(start: number, callback: (message: any) => void, cancellationToken: ICancellationToken): void;
}

class StreamReader implements IStreamReader {
    count: number = 20;
    entityName: string;
    entityIdentity: string;
    host: string;
    service: IServiceDependency;
    constructor(options: IEventStoreClientOptions, entityName: string, service: IServiceDependency, entityIdentity?: string) {
        this.entityName = entityName;
        this.service = service;
        this.entityIdentity = entityIdentity;
        this.host = `http://${options.hostname}:${options.port}`;
    }

    _removeHost(url: string): string {
        return url.replace(this.host, '');
    }

    async _readEvent(path: string) {
        return await this.service.get(path);
    }


    _getLink(streamInfo: IStreamInfo, relation: string): string {
        if (streamInfo && streamInfo.links) {
            let filter = streamInfo.links.filter(l => l.relation === relation);
            if (filter && filter.length > 0) {
                return filter[0].uri;
            }
        }
        return null;
    }

    async getStreamInfo(path: string, longPolling: boolean = false, cancellationToken?: ICancellationToken): Promise<IStreamInfo> {
        if (path == null) {
            return null;
        }
        let fixedPath = this._removeHost(path);
        const requestOptions = {
            headers: {
                "ES-LongPoll": "10"
            }
        };
        let response = await this.service.get<IStreamInfo>(fixedPath, requestOptions, cancellationToken);
        return response.result;
    }


    async getEventTask(url: string, eventNumber: number): Promise<IEvent> {
        let link = urlBuilder(eventUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber });
        let index = url.lastIndexOf('/');
        let originalNumber = Number.parseInt(url.substring(index + 1));
        let event = await this._readEvent(this._removeHost(url));
        return {
            data: event.result,
            originalEventNumber: originalNumber,
            link: link,
            eventNumber,
            originalLink: url
        };
    }

    async tryGetEntries(streamInfo: IStreamInfo, counter: ICounter): Promise<IEvent[]> {
        let task: Promise<IEvent>[] = [];
        counter.streamRead();
        if (streamInfo.entries && streamInfo.entries) {
            counter.incomingEntries(streamInfo.entries.length);
            for (let entry of streamInfo.entries) {
                counter.next();
                task.push(this.getEventTask(entry.id, counter.current));
            }
        }
        return await Promise.all(task);
    }

    async readForward(start: number = 0, callback: (message: any) => void): Promise<void> {
        let counter = new ForwardCounter(start, this.count);
        let path = urlBuilder(forwardUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber: start, count: this.count });
        let streamInfo = await this.getStreamInfo(path);
        while (streamInfo) {
            let entries = await this.tryGetEntries(streamInfo, counter);
            for (let i = entries.length - 1; i >= 0; i--) {
                callback(entries[i]);
            }
            let previousPath = this._getLink(streamInfo, 'previous');
            streamInfo = await this.getStreamInfo(previousPath);
        }
    }


    async _getPreviousOrWait(path: string, counter: ICounter, callback: (message: any) => void, cancellationToken: ICancellationToken) {
        let running = true;
        let subscription = cancellationToken.onCancel(() => { running = false });
        while (running) {
            try {
                let streamInfo = await this.getStreamInfo(path, true, cancellationToken);
                let entries = await this.tryGetEntries(streamInfo, counter);
                if (entries) {
                    for (let i = entries.length - 1; i >= 0; i--) {
                        callback(entries[i]);
                    }
                }
                let previousPath = this._getLink(streamInfo, 'previous');
                path = previousPath ? previousPath : path;
            }
            catch (error) {
                if (running) {
                    throw error;
                }
            }
        }
        subscription.clear();
    }


    followFrom(start: number, callback: (message: any) => void, cancellationToken: ICancellationToken): void {
        let counter = new ForwardCounter(start, this.count);
        let path = urlBuilder(forwardUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber: start, count: this.count });
        this._getPreviousOrWait(path, counter, callback, cancellationToken);

    }

    async readBackward(start: number = 0, callback: (message: any) => void): Promise<void> {
        let counter = new BackwardsCounter(start, this.count);
        let path = urlBuilder(backwardsUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber: start, count: this.count });
        let streamInfo = await this.getStreamInfo(path);
        while (streamInfo) {

            let entries = await this.tryGetEntries(streamInfo, counter);
            for (let i = 0; i < entries.length; i++) {
                callback(entries[i]);
            }
            let previousPath = this._getLink(streamInfo, 'next');
            streamInfo = await this.getStreamInfo(previousPath);
        }
    }
}



export class EventStoreClient implements IAvailability {
    options: IEventStoreClientOptions;
    serviceDependency: IServiceDependency;
    constructor(){
    }

    static DefaultOptions: IEventStoreClientOptions = {
        hostname: 'localhost',
        port: 2113
    }

    streamReader(entityName: string, entityIdentity?: string): IStreamReader {
        return new StreamReader(this.options, entityName, this.serviceDependency, entityIdentity);
    }

    async emit(entityName: string, entityIdentity: string, expectedVersion: number, entity: any, eventType: string) {
        let eventId = crypto.randomBytes(16).toString("hex");
        let path = urlBuilder(streamUri, { entityName, entityIdentity });
        const requestOptions = {
            headers: {
                "Content-Type": "application/json",
                "ES-EventType": eventType,
                "ES-EventId": eventId
            }
        };
        return await this.serviceDependency.post(path, entity, requestOptions);
    }


    name: string;
    isAvailable: boolean;
    init (options: IEventStoreClientOptions) {
        this.options = extend(true, EventStoreClient.DefaultOptions, options);
        this.serviceDependency.init(options);
    }

    stop () {
        this.serviceDependency.stop();
    }

    stateChanged (callback: CallbackFunction){
        this.serviceDependency.stateChanged(callback);    
    }
}




