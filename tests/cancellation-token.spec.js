'use strict';
const expect = require('chai').expect;
const CancellationToken = require('../src/cancellation-token').CancellationToken;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


async function CancellableTask(cancellationToken, callBack) {

    let isRunning = true;
    let subscription = cancellationToken.onCancel(() => {
        isRunning = false;
    });
    while (isRunning) {
        await sleep(10);
    }
    callBack(subscription);

}

describe("Cancellation token", () => {

    it("Can cancel a long running task", (complete) => {
        let token = new CancellationToken();

        let called = false;
        let timeout = setTimeout(() => {
            called = true
        }, 200);
        setTimeout(() => {
            expect(called).equals(false);
            complete();
        }, 210);
        token.onCancel((reason) => {
            clearTimeout(timeout);
            expect(reason).equals("Stop it");
        });
        token.cancel("Stop it");
    });


    it("Does nothing if the tasks has completed.", (complete) => {
        let token = new CancellationToken();
        let subscription = null;
        let called = false;
        let timeout = setTimeout(() => {
            called = true;
            subscription.clear();
        }, 150);
        setTimeout(() => token.cancel("Stop it"), 160);
        setTimeout(() => {
            expect(called).equals(true);
            complete();
        }, 210);
        subscription = token.onCancel((reason) => {
            expect.fail("This should never be called. Task has finished.")
        });;
    });


    it("Cancels long running task", (complete) => {
        let token = new CancellationToken();
        let taskEnded = false;


        setTimeout(() => {
            let start = Date.now();
            token.cancel().then(() => {
                expect(taskEnded).equals(true);
                expect(Date.now() - start).to.be.lessThan(50);
                complete();
            });
        }, 100);

        CancellableTask(token, async (subscription) => {
            taskEnded = true;
            subscription.clear();
        });
    });

    it("Timeout if the subscription is not cleared.", (complete) => {
        let token = new CancellationToken();
        let taskEnded = false;


        setTimeout(() => {
            let start = Date.now();
            token.cancel().then(() => {
                expect(taskEnded).equals(true);
                expect(Date.now() - start).to.be.greaterThan(1000);
                expect(Date.now() - start).to.be.lessThan(1050);
                complete();
            });
        }, 100);

        CancellableTask(token, async (subscription) => {
            taskEnded = true;
        });
    });
});