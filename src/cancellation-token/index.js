'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter = require("events");
class Subscription {
    constructor(callback, cancellationToken) {
        this.cancellationToken = cancellationToken;
        this.callback = (reason) => {
            callback(reason);
            setTimeout(() => this.finished(), 1000);
        };
        this.waiter = new Promise(resolve => {
            this.finished = () => {
                resolve();
            };
        });
    }
    clear() {
        this.finished();
        this.cancellationToken._removeInternal(this);
    }
}
class CancellationToken {
    constructor() {
        this._subscriptions = [];
        this._eventEmitter = new EventEmitter();
    }
    cancel(reason) {
        return __awaiter(this, void 0, void 0, function* () {
            let tasks = this._subscriptions.map(s => s.waiter);
            this._eventEmitter.emit('cancel_called', reason);
            yield Promise.all(tasks);
        });
    }
    onCancel(callback) {
        console.log('Subs: ' + this._eventEmitter.listenerCount('cancel_called'));
        let subscription = new Subscription(callback, this);
        this._eventEmitter.on('cancel_called', subscription.callback);
        this._subscriptions.push(subscription);
        return subscription;
    }
    _removeInternal(subscription) {
        let index = this._subscriptions.indexOf(subscription);
        this._subscriptions.splice(index, 1);
        this._eventEmitter.removeListener('cancel_called', subscription.callback);
    }
    remove(subscription) {
        subscription.clear();
    }
}
exports.CancellationToken = CancellationToken;
//# sourceMappingURL=index.js.map