'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const https = require("https");
const http = require("http");
function makeRequest(options, body, token) {
    return new Promise((accept, reject) => {
        let subscription = null;
        const req = http.request(options, (res) => {
            let statusCode = res.statusCode;
            let headers = res.headers;
            let chunk = "";
            res.on('data', (d) => {
                chunk += d;
            });
            res.on('end', () => {
                if (subscription) {
                    subscription.clear();
                }
                let json = chunk && chunk.length ? JSON.parse(chunk) : null;
                return accept({ statusCode, headers, result: json });
            });
        });
        req.on('error', (e) => {
            if (subscription) {
                subscription.clear();
            }
            return reject(e);
        });
        if (body) {
            req.write(JSON.stringify(body));
        }
        req.end();
        if (token) {
            subscription = token.onCancel(() => {
                req.abort();
            });
        }
    });
}
exports.makeRequest = makeRequest;
function makeSecureRequest(options, body, token) {
    return new Promise((accept, reject) => {
        const req = https.request(options, (res) => {
            let statusCode = res.statusCode;
            let headers = res.headers;
            let chunk = "";
            res.on('data', (d) => {
                chunk += d;
            });
            res.on('end', () => {
                let json = chunk && chunk.length ? JSON.parse(chunk) : null;
                return accept({ statusCode, headers, result: json });
            });
        });
        req.on('error', (e) => {
            return reject(e);
        });
        if (body) {
            req.write(JSON.stringify(body));
        }
        req.end();
        if (token) {
            token.onCancel(() => req.abort());
        }
    });
}
exports.makeSecureRequest = makeSecureRequest;
//# sourceMappingURL=request-helper.js.map