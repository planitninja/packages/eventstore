'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const event_store_helper_1 = require("./event-store-helper");
const cancellation_token_1 = require("./cancellation-token");
let token = new cancellation_token_1.CancellationToken();
function End() {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('starting: ' + Date.now());
        yield token.cancel('ending');
        console.log('done: ' + Date.now());
    });
}
(() => __awaiter(this, void 0, void 0, function* () {
    let client = new event_store_helper_1.EventStoreClient();
    let reader = client.streamReader('entity');
    reader.followFrom(0, m => console.log('aa: ' + JSON.stringify(m)), token);
    yield reader.readBackward(100, m => console.log(JSON.stringify(m)));
    setTimeout(() => End(), 2000);
}))();
//# sourceMappingURL=index.js.map