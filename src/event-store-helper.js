'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const extend = require("extend");
const crypto = require("crypto");
const request_helper_1 = require("./request-helper");
const forwardUri = { category: '/streams/$ce-{entityName}/{start}/forward/{count}', entity: '/streams/{entityName}-{entityIdentity}/{start}/forward/{count}' };
const backwardsUri = { category: '/streams/$ce-{entityName}/{start}/backward/{count}', entity: '/streams/{entityName}-{entityIdentity}/{start}/backward/{count}' };
const streamUri = { category: '/streams/$ce-{entityName}', entity: '/streams/{entityName}-{entityIdentity}' };
const eventUri = { category: '/streams/$ce-{entityName}/{start}', entity: '/streams/{entityName}-{entityIdentity}/{start}' };
class ForwardCounter {
    constructor(start, streamCount) {
        this.current = start;
        this.skip = streamCount;
    }
    streamRead() {
    }
    incomingEntries(count) {
        this.current += count;
    }
    next() {
        this.current--;
    }
}
class BackwardsCounter {
    constructor(start, streamCount) {
        this.started = false;
        this.current = start + 1;
        this.skip = streamCount;
    }
    streamRead() {
        if (this.started == false) {
            this.current -= this.skip;
        }
    }
    incomingEntries(count) {
        if (count > 0 && this.started === false) {
            this.current += count;
            this.started = true;
        }
    }
    next() {
        this.current--;
    }
}
function urlBuilder(template, params) {
    if (template === undefined || template === null) {
        throw new Error("Entity name required");
    }
    if (params === undefined || params === null) {
        throw new Error("Entity name required");
    }
    if (params.entityName === undefined || params.entityName === null) {
        throw new Error("Entity name required");
    }
    let url = params.entityIdentity ? template.entity : template.category;
    url = url.replace('{entityName}', params.entityName);
    if (params.entityIdentity) {
        url = url.replace('{entityIdentity}', params.entityIdentity);
    }
    let eventNumber = params.eventNumber || 0;
    url = url.replace('{start}', eventNumber.toString());
    let count = params.count || 20;
    url = url.replace('{count}', count.toString());
    return url;
}
;
class StreamReader {
    constructor(options, entityName, entityIdentity) {
        this.count = 20;
        this.entityName = entityName;
        this.entityIdentity = entityIdentity;
        this.options = options;
        this.host = `http://${this.options.hostname}:${this.options.port}`;
    }
    _requestOptions(options) {
        let root = {
            hostname: this.options.hostname,
            port: this.options.port
        };
        return extend(true, root, options);
    }
    _removeHost(url) {
        return url.replace(this.host, '');
    }
    _readEvent(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestOptions = this._requestOptions({
                path,
                method: 'GET',
                headers: {
                    "Accept": "application/json"
                }
            });
            return yield request_helper_1.makeRequest(requestOptions);
        });
    }
    _getLink(streamInfo, relation) {
        if (streamInfo && streamInfo.links) {
            let filter = streamInfo.links.filter(l => l.relation === relation);
            if (filter && filter.length > 0) {
                return filter[0].uri;
            }
        }
        return null;
    }
    getStreamInfo(path, longPolling = false, cancellationToken) {
        return __awaiter(this, void 0, void 0, function* () {
            if (path == null) {
                return null;
            }
            let fixedPath = this._removeHost(path);
            const requestOptions = this._requestOptions({
                path: fixedPath,
                method: 'GET',
                headers: {
                    "Accept": "application/json"
                }
            });
            if (longPolling) {
                requestOptions.headers["ES-LongPoll"] = "10";
            }
            let response = yield request_helper_1.makeRequest(requestOptions, null, cancellationToken);
            return response.result;
        });
    }
    getEventTask(url, eventNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            let link = urlBuilder(eventUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber });
            let index = url.lastIndexOf('/');
            let originalNumber = Number.parseInt(url.substring(index + 1));
            let event = yield this._readEvent(this._removeHost(url));
            return {
                data: event.result,
                originalEventNumber: originalNumber,
                link: link,
                eventNumber,
                originalLink: url
            };
        });
    }
    tryGetEntries(streamInfo, counter) {
        return __awaiter(this, void 0, void 0, function* () {
            let task = [];
            counter.streamRead();
            if (streamInfo.entries && streamInfo.entries) {
                counter.incomingEntries(streamInfo.entries.length);
                for (let entry of streamInfo.entries) {
                    counter.next();
                    task.push(this.getEventTask(entry.id, counter.current));
                }
            }
            return yield Promise.all(task);
        });
    }
    readForward(start = 0, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let counter = new ForwardCounter(start, this.count);
            let path = urlBuilder(forwardUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber: start, count: this.count });
            let streamInfo = yield this.getStreamInfo(path);
            while (streamInfo) {
                let entries = yield this.tryGetEntries(streamInfo, counter);
                for (let i = entries.length - 1; i >= 0; i--) {
                    callback(entries[i]);
                }
                let previousPath = this._getLink(streamInfo, 'previous');
                streamInfo = yield this.getStreamInfo(previousPath);
            }
        });
    }
    _getPreviousOrWait(path, counter, callback, cancellationToken) {
        return __awaiter(this, void 0, void 0, function* () {
            let running = true;
            let subscription = cancellationToken.onCancel(() => { running = false; });
            while (running) {
                try {
                    let streamInfo = yield this.getStreamInfo(path, true, cancellationToken);
                    let entries = yield this.tryGetEntries(streamInfo, counter);
                    if (entries) {
                        for (let i = entries.length - 1; i >= 0; i--) {
                            callback(entries[i]);
                        }
                    }
                    let previousPath = this._getLink(streamInfo, 'previous');
                    path = previousPath ? previousPath : path;
                }
                catch (error) {
                    if (running) {
                        throw error;
                    }
                }
            }
            subscription.clear();
        });
    }
    followFrom(start, callback, cancellationToken) {
        let counter = new ForwardCounter(start, this.count);
        let path = urlBuilder(forwardUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber: start, count: this.count });
        this._getPreviousOrWait(path, counter, callback, cancellationToken);
    }
    readBackward(start = 0, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let counter = new BackwardsCounter(start, this.count);
            let path = urlBuilder(backwardsUri, { entityName: this.entityName, entityIdentity: this.entityIdentity, eventNumber: start, count: this.count });
            let streamInfo = yield this.getStreamInfo(path);
            while (streamInfo) {
                let entries = yield this.tryGetEntries(streamInfo, counter);
                for (let i = 0; i < entries.length; i++) {
                    callback(entries[i]);
                }
                let previousPath = this._getLink(streamInfo, 'next');
                streamInfo = yield this.getStreamInfo(previousPath);
            }
        });
    }
}
class EventStoreClient {
    constructor(options) {
        this.options = extend(true, EventStoreClient.DefaultOptions, options);
    }
    _categoryUrlBuilder(template, entityName, eventNumber, count) {
        let url = template.replace('{entityName}', entityName);
        url = url.replace('{start}', eventNumber.toString());
        url = url.replace('{count}', count.toString());
        return url;
    }
    _requestOptions(options) {
        let root = {
            hostname: this.options.hostname,
            port: this.options.port
        };
        return extend(true, root, options);
    }
    streamReader(entityName, entityIdentity) {
        return new StreamReader(this.options, entityName, entityIdentity);
    }
    emit(entityName, entityIdentity, expectedVersion, entity, eventType) {
        return __awaiter(this, void 0, void 0, function* () {
            let eventId = crypto.randomBytes(16).toString("hex");
            let path = urlBuilder(streamUri, { entityName, entityIdentity });
            const requestOptions = this._requestOptions({
                path,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "ES-EventType": eventType,
                    "ES-EventId": eventId
                }
            });
            return yield request_helper_1.makeRequest(requestOptions, entity);
        });
    }
}
EventStoreClient.DefaultOptions = {
    hostname: 'localhost',
    port: 2113
};
exports.EventStoreClient = EventStoreClient;
//# sourceMappingURL=event-store-helper.js.map